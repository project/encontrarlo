/**
 * @file
 * Swaps word Registration out for Application when it's application required.
 *
 * @TODO find/replace *translated* strings, not just English.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.swapRegistrationLabelsForApplication = {
    attach: function (context) {
      $("#edit-field-findit-registration-type").on("change",function() {
        if ($("#edit-field-findit-registration-type-application:checked").val()) {
          $('#edit-field-findit-registration-when--wrapper legend span').text("Application availability");
          $('#edit-field-findit-registration-notes-wrapper div div label').text("Application instructions");
          $('#edit-field-findit-registration-url-0 legend span').text("Application URL");
          $('#edit-field-findit-registration-file-0--label').text("Application file");
        } else {
          // If the value isn't application, put everything back where we found it.
          $('#edit-field-findit-registration-when--wrapper legend span').text("Registration availability");
          $('#edit-field-findit-registration-notes-wrapper div div label').text("Registration instructions");
          $('#edit-field-findit-registration-url-0 legend span').text("Registration URL");
          $('#edit-field-findit-registration-file-0--label').text("Registration file");

        }
        // The trigger handler ensures it runs on page load (when already set).
      }).triggerHandler("change");
    }
  };

})(jQuery, Drupal);
