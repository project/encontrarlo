/**
 * @file
 * Copy time value when adding an additional date.
 */

(function ($, Drupal) {

  $(document).ajaxComplete(function(event, request, settings) {
    // We could make this run slightly less often by drilling down into this object.
    // request.responseJSON[0]
    // specifically for:​
    // settings: ajax: callback: Array [ "Drupal\\datetime_range\\Plugin\\Field\\FieldWidget\\DateRangeDefaultWidget", "addMoreAjax" ]
    var contestants = $('[name^="field_findit_dates["][name$="][value][time]"]');
    var endtime_contestants = $('[name^="field_findit_dates["][name$="][end_value][time]"]');
    var last_time = '';
    contestants.each(function() {
      var name = $(this).attr('name');
      if (last_time && !$(this).val()) {
        $(this).val(last_time);
      }
      last_time = $(this).val();
    });
    var last_time = '';
    endtime_contestants.each(function() {
      var name = $(this).attr('name');
      if (last_time && !$(this).val()) {
        $(this).val(last_time);
      }
      last_time = $(this).val();
    });
  });

})(jQuery, Drupal);
